C:\xampp>cd mysql
C:\xampp\mysql>cd bin
C:\xampp\mysql\bin>mysql -uroot


Soal 1 Membuat Database
	MariaDB [(none)]> create database myshop;

Soal 2 Membuat Table di Dalam Database
	MariaDB [myshop]> create table users (
    	-> id int(8) auto_increment,
    	-> name varchar(255),
    	-> email varchar(255),
    	-> password varchar(255),
    	-> primary key(id)
    	-> );

	MariaDB [myshop]> create table categories (
   	 -> id int(8) auto_increment,
   	 -> name varchar(255),
  	 -> primary key(id)
    	 -> );

	MariaDB [myshop]> create table items (
    	-> id int(8) auto_increment,
    	-> name varchar(255),
    	-> description varchar(255),
    	-> price int(8),
    	-> stock int(8),
    	-> category_id int(8),
    	-> primary key(id),
    	-> foreign key(category_id) references categories(id)
    	-> );


Soal 3 Memasukkan Data pada Table
	users
	MariaDB [myshop]> insert into users(name,email,password) values("John Doe","john@doe.com","john123"),
                  ("Jane Doe","jane@doe.com","jenita123");


	categories
	MariaDB [myshop]> insert into categories(name) values ("gadget"),("cloth"),("men"),("women"),("branded");

	items 
	MariaDB [myshop]> insert into items(name, description, price, stock, category_id) values("sumsang b50",
                  "hape keren dari merek sumsang", 4000000, 100,1),("uniklooh","baju keren dari brand ternama",
                    500000, 50, 2),("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10,1);


Soal 4 Mengambil Data dari Database
	a. Mengambil data users
		Buatlah sebuah query untuk mendapatkan data seluruh user pada table users. Sajikan semua field pada table users KECUALI password nya.

	MariaDB [myshop]> select id, name, email from users;


	b. Mengambil data items
		Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).

	MariaDB [myshop]> select * from items where > 1000000;

	Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, 
	atau “sang” (pilih salah satu saja).
 
	MariaDB [myshop]> select * from items where name like '%sang%' ;

	c. Menampilkan data items join dengan kategori

	MariaDB [myshop]> select items.id, items.description, items.price, items.stock, items.category_id, categories.name  from items inner join categories on items.id  = categories.id;


Soal 5 Mengubah Data dari Database
	Ubahlah data pada table items untuk item dengan nama sumsang b50 harganya (price) menjadi 2500000.

	MariaDB [myshop]> update items set price =  2500000 where id=1;